import { createRouter, createWebHistory } from 'vue-router'

//引入布局组件
import Layout from '../views/Layout/index.vue'
const routes = [
  {
    path: '/',
    name: 'Login',
    hidden: true,
    meta: {
      name: '登录'
    },
    //按需加载
    component: () => import('../views/Login.vue')
  },
  {
    path: '/:catchAll(.*)',
    name: '404',
    hidden: true,
    meta: {
      name: '404页面'
    },
    //按需加载
    component: () => import('../views/404/404.vue')
  },

  {
    path: '/carousel',
    name: 'system',
    //重定向
    meta: {
      name: '公告管理',
      icon: 'MessageBox'
    },
    //控制台
    component: Layout,
    children: [
      {
        path: '/notice',
        name: 'Notice',
        meta: {
          name: '医院公告管理',
        },
        component: () => import('../views/System/notice.vue')
      },
      
      {
        path: '/carousel',
        name: 'Carousel',
        meta: {
          name: '轮播图预览',
        },
        component: () => import('../views/System/carousel.vue')
      }
      //新增
    ]
  },

   {
    path: '/article',
    name: 'Article',
    //重定向
    meta: {
      name: '文章管理',
      icon: 'ChatLineSquare'
    },
    //控制台
    component: Layout,
    children: [
      {
        path: '/addArticle',
        name: 'AddArticle',
        meta: {
          name: '添加文章',
        },
        component: () => import('../views/Article/article-form.vue')
      },
      {
        path: '/articleList',
        name: 'ArticleList',
        meta: {
          name: '文章列表',
        },
        component: () => import('../views/Article/article-list.vue')
      }
    ]
  },

  {
    path: '/departAdmin622',
    name: 'System3',
    redirect: 'index',
    //从定向
    meta: {
      name: '实践学期医院管理',
      icon: 'School'
    },
    //控制台
    component: Layout,
    children: [
      {
        path: '/departAdmin622',
        name: 'wanquanbutong_622',
        meta: {
          name: '科室管理',
        },
        component: () => import('../views/Depart/department_wyl.vue')
      },
      {
        path: '/departAdmin623',
        name: 'room_wyl',
        meta: {
          name: '诊室管理',
        },
        component: () => import('../views/Depart/room_wyl.vue')
      },
      {
        path: '/departAdmin624',
        name: '404_wyl',
        meta: {
          name: '挂号费管理',
        },
        component: () => import('../views/Depart/404_wyl.vue')
      },
      {
        path: '/departAdmin625',
        name: 'user_wyl',
        meta: {
          name: '用户管理',
        },
        component: () => import('../views/Depart/user_wyl.vue')
      },
      {
        path: '/departAdmin626',
        name: '404_wyl_2',
        meta: {
          name: '用户角色管理',
        },
        component: () => import('../views/Depart/404_wyl_2.vue')
      },
      //{
     //   path: '/zhuce',
     //   name: 'zhuce',
    //    meta: {
    //      name: '注册',
    //    },
     //   component: () => import('../views/User/zhuce.vue')
     // }
    ]
  },
  {
    path: '/carouse2',
    name: 'system3',
    redirect: 'index',
    //从定向
    meta: {
      name: '医生管理',
      icon: 'HotWater'
    },
    //控制台
    component: Layout,
    children: [
      {
        path: '/doctor',
        name: 'Doctor',
        meta: {
          name: '医生管理',
        },
        component: () => import('../views/Doctor/Doctor_demo.vue')
      },
      {
        path: '/schedule',
        name: '排班管理',
        meta: {
          name: '排班管理',
        },
        //component: () => import('../views/Doctor/schedule_Admin_wyl.vue')
        component: () => import('../views/Doctor/orign_schedule.vue')
      }
    ]
  },
  {
    path: '/nat',
    name: 'natAdmin',
    redirect: 'index',
    //从定向
    meta: {
      name: '核酸管理',
      icon: 'IceCreamRound'
    },
    //控制台
    component: Layout,
    children: [
      {
        path: '/natpatientAdmin',
        name: 'natpatient_admin',
        meta: {
          name: '患者核酸信息',
        },
        component: () => import('../views/Nat/natpatient.vue')
      }
    ]
  },
  {
    path: '/carouse3',
    name: 'system4',
    redirect: 'index',
    //从定向
    meta: {
      name: '实践学期优化新功能',
      icon: 'StarFilled'
    },
    //控制台
    component: Layout,
    children: [
      {
        path: '/special',
        name: 'Special',
        meta: {
          name: '白名单管理',
        },
        component: () => import('../views/wangyinlin/special.vue')
      },
      {
        path: '/apply',
        name: 'apply',
        meta: {
          name: '特殊用户申请',
        },
        component: () => import('../views/wangyinlin/apply.vue')
      },
      {
        path: '/system_3_schedule',
        name: 'system_3_schedule',
        meta: {
          name: '查房管理',
        },
        component: () => import('../views/Depart/bed.vue')
      },
      {
        path: '/WaitSee',
        name: 'WaitSee',
        meta: {
          name: '候诊查询',
        },
        component: () => import('../views/WaitSee/WaitSee.vue')
      },
      {
        path: '/jiaohao',
        name: 'jiaohao',
        meta: {
          name: '模拟叫号',
        },
        component: () => import('../views/WaitSee/jiaohao.vue')
      }
    ]
  },
  

]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})


export default router
